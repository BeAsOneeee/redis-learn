package com.learn;

import redis.clients.jedis.Jedis;

public class App {
    public static void main(String[] args) {
        App app=new App();
        app.Set("sno","110");
        app.Del("sno");

        final String val = app.Get("sno");
        System.out.println(val);
    }

    public void Set(String key,String val){
        final Jedis resource = RedisFactory.getInstance().getResource();
        try{
            resource.set(key,val);
        }finally {
            resource.close();
        }
    }

    public String Get(String key){
        final Jedis resource = RedisFactory.getInstance().getResource();
        try{
            resource.expire(key, 1800);
            final String val = resource.get(key);
            return val;
        }finally {
            resource.close();
        }
    }

    public void Del(String key){
        final Jedis resource = RedisFactory.getInstance().getResource();
        try{
            resource.del(key);
        }finally {
            resource.close();
        }
    }
}
